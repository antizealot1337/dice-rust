# Dice

A dice roller command line program written in [Rust]. The program can "roll" a
die of any size and any number. It can also total all the rolls.

## Why?

This is mostly an excersize in learning Rust and [fastrand].

## Usage

While the primary usage of this project is as a command line dice roller, it
does also provide a library api for quick dice rolling in other applications.

### Command line Usage

```text
Dice rolling tool

Usage: dice [OPTIONS]

Options:
  -s, --sides <sides>  The number of sides of the die [default: 6]
  -t, --times <times>  The number of times to roll [default: 1]
      --total          Total all the rolls together
  -h, --help           Print help information
  -V, --version        Print version informatio
```

### Library

The library is divided into 2 modules: `roll` and `flip`.

#### Module dice::roll

The `roll` module contains functions to quickly roll for 4, 6, 8, 10, 12, 20, and 100 sided die. It also has a general purpose function `die_with_sides` for rolling a n-sided die. The general purpose function can fail if an invalid value is provided.

```rust
use dice::roll;

fn example() {
  let four_sided_roll = roll::d4();

  let n_sided_roll = roll::die_with_sides(11).unwrap();
}
```

#### Module dice::flip

The `flip` module contains the `coin` function for flipping a coin.

```rust
use dice::flip;


fn example() {
  let heads = flip::coin();
}
```

## Copyright
Copyright Christopher Pridgen and provided under the terms of the MIT license.

See LICENSE for more information

[Rust]: https://rust-lang.org/
[fastrand]: https://crates.io/crates/fastrand
