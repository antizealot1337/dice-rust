/// Flip a coin and return true of false. These can be interpreted as heads and
/// tails however is desired.
pub fn coin() -> bool {
    fastrand::bool()
} //fn

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_flip_coin() {
        for _ in 0..5 {
            let _ = coin();
        } //for
    } //fn
} //mod
