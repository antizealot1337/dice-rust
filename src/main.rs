use clap::{Arg, ArgAction, Command};
use dice::roll;

const ARG_SIDES: &'static str = "sides";
const ARG_TIMES: &'static str = "times";
const ARG_TOTAL: &'static str = "total";

fn main() {
    let matches = Command::new("Dice")
        .about("Dice rolling tool")
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .arg(
            Arg::new(ARG_SIDES)
                .action(ArgAction::Set)
                .long("sides")
                .short('s')
                .help("The number of sides of the die")
                .default_value("6")
                .value_parser(clap::value_parser!(i32).range(2..)),
        )
        .arg(
            Arg::new(ARG_TIMES)
                .action(ArgAction::Set)
                .long("times")
                .short('t')
                .help("The number of times to roll")
                .default_value("1")
                .value_parser(clap::value_parser!(i32).range(1..)),
        )
        .arg(
            Arg::new(ARG_TOTAL)
                .action(ArgAction::SetTrue)
                .long("total")
                .help("Total all the rolls together")
                .alias("sum"),
        )
        .get_matches();

    let sides = matches
        .get_one::<i32>(ARG_SIDES)
        .expect("invalid sides value");
    let times = matches
        .get_one::<i32>(ARG_TIMES)
        .expect("invalid times value");
    let total = matches.get_flag(ARG_TOTAL);

    if *times == 1 {
        println!("{}", roll::die_with_sides(*sides).unwrap());
    } else {
        let mut sum = 0;

        print!("Rolls: [");

        (0..*times).for_each(|i| {
            let current = roll::die_with_sides(*sides).unwrap();

            if i + 1 < *times {
                print!("{current}, ");
            } else {
                print!("{current}");
            }

            if total {
                sum += current;
            }
        });

        println!("]");

        if total {
            println!("Total: {sum}");
        } //if
    } //if
} //fn
