#[derive(Debug, PartialEq)]
pub struct RollError;

/// Roll a die with the specified sides. The number of sides should be greater
/// than 1 or a RollError will occur.
pub fn die_with_sides(sides: i32) -> Result<i32, RollError> {
    if sides > 1 {
        Ok(fastrand::i32(1..sides + 1))
    } else {
        Err(RollError)
    } //if
} //fn

/// Roll a D4. This is the same as calling roll(4) and unwraping the error.
pub fn d4() -> i32 {
    die_with_sides(4).unwrap()
} //fn

/// Roll a D6. This is the same as calling roll(6) and unwraping the error.
pub fn d6() -> i32 {
    die_with_sides(6).unwrap()
} //fn

/// Roll a D8. This is the same as calling roll(8) and unwraping the error.
pub fn d8() -> i32 {
    die_with_sides(8).unwrap()
} //fn

/// Roll a D10. This is the same as calling roll(10) and unwraping the error.
pub fn d10() -> i32 {
    die_with_sides(10).unwrap()
} //fn

/// Roll a D12. This is the same as calling roll(12) and unwraping the error.
pub fn d12() -> i32 {
    die_with_sides(12).unwrap()
} //fn

/// Roll a D20. This is the same as calling roll(20) and unwraping the error.
pub fn d20() -> i32 {
    die_with_sides(20).unwrap()
} //fn

/// Roll a D100. This is the same as calling roll(100) and unwraping the error.
pub fn d100() -> i32 {
    die_with_sides(100).unwrap()
} //fn

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_roll_bad_sides() {
        assert_eq!(die_with_sides(1), Err(RollError));
    } //fn

    #[test]
    fn test_d4() {
        for _ in 0..8 {
            let num = d4();

            assert!(num > 0 && num < 5);
        } //for
    } //fn

    #[test]
    fn test_d6() {
        for _ in 0..12 {
            let num = d6();

            assert!(num > 0 && num < 7);
        } //for
    } //fn

    #[test]
    fn test_d8() {
        for _ in 0..16 {
            let num = d8();

            assert!(num > 0 && num < 9);
        } //for
    } //fn

    #[test]
    fn test_d10() {
        for _ in 0..20 {
            let num = d10();

            assert!(num > 0 && num < 11);
        } //for
    } //fn

    #[test]
    fn test_d12() {
        for _ in 0..24 {
            let num = d12();

            assert!(num > 0 && num < 13);
        } //for
    } //fn

    #[test]
    fn test_d20() {
        for _ in 0..40 {
            let num = d20();

            assert!(num > 0 && num < 21);
        } //for
    } //fn

    #[test]
    fn test_d100() {
        for _ in 0..150 {
            let num = d100();

            assert!(num > 0 && num < 101);
        } //for
    } //fn
} //mod
